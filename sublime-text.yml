---
-
  hosts: 127.0.0.1
  connection: local
  vars:
    install_sublime: false

  tasks:
    -
      name: 'Add sublimetext repository'
      become: yes
      yum_repository:
        name: sublime-repository
        description: Sublime repository
        baseurl: https://download.sublimetext.com/rpm/stable/x86_64/
        enabled: yes
        state: present
        gpgkey: https://download.sublimetext.com/sublimehq-rpm-pub.gpg
        gpgcheck: yes
      when: install_sublime
      register: install_sublime_result

    -
      name: 'Install sublime text'
      become: yes
      package:
        state: present
        name: sublime-text
      when: install_sublime and install_sublime_result is succeeded

    -
      name: 'Ensure sublime text installed packages directory exists'
      file:
        path: '~/.config/sublime-text-3/Installed Packages'
        state: directory

    -
      name: 'Download sublime text package control'
      get_url:
        url: 'https://packagecontrol.io/Package%20Control.sublime-package'
        dest: '~/.config/sublime-text-3/Installed Packages/Package Control.sublime-package'
        checksum: sha256:6f4c264a24d933ce70df5dedcf1dcaeeebe013ee18cced0ef93d5f746d80ef60

    -
      name: 'Ensure sublime text package user settings directory exists'
      file:
        path: '~/.config/sublime-text-3/Packages/User/'
        state: directory

    -
      name: 'Create and populate package control user config file'
      copy:
        dest: '~/.config/sublime-text-3/Packages/User/Package Control.sublime-settings'
        force: no
        content: |
          {
            "installed_packages":
            [
              "Compare Side-By-Side",
              "ExportHtml",
              "Fortran",
              "LSP",
              "MarkdownLivePreview",
              "Package Control",
              "Solarized Color Scheme"
            ]
          }

    -
      name: 'Sublime text config'
      copy:
        dest: '~/.config/sublime-text-3/Packages/User/Preferences.sublime-settings'
        force: no
        content: |
          {
            "color_scheme": "Packages/Solarized Color Scheme/Solarized (light).sublime-color-scheme",
            "dictionary": "Packages/Language - English/en_GB.dic",
            "font_size": 14,
            "word_wrap": "false"
          }

    -
      name: 'Create and config LSP package'
      copy:
        dest: '~/.config/sublime-text-3/Packages/User/LSP.sublime-settings'
        force: no
        content: |
          {
            "clients":
              {
              "fortran":
                {
                  "command":
                  [
                    "fortls",
                    "--symbol_skip_mem",
                    "--incrmental_sync",
                    "--autocomplete_no_prefix"
                  ],
                  "enabled": true,
                  "languageId": "fortran",
                  "scopes":
                  [
                    "source.fixedform-fortran"
                  ],
                  "syntaxes":
                  [
                    "Packages/Fortran/grammars/FortranFixedForm.sublime-syntax"
                  ]
              }
            }
          }

    -
      name: 'Create and config fortran config'
      copy:
        dest: '~/.config/sublime-text-3/Packages/User/FortranFixedForm.sublime-settings'
        force: no
        content: |
          {
            "extensions":
            [
              "fi"
            ]
          }
